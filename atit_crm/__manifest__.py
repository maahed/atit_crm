# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'ATIT CRM',
    'version' : '1.0',
    'sequence': 40,
    'description': """
    """,
    'category': 'CRM',
    'depends' : ['crm', 'sale', 'account'],
    'data': [
        'views/crm_view.xml',
        'views/res_config_setting.xml',
        'report/layout_template.xml',
        'report/invoice_report.xml',
    ],
    'installable': True,
    'application': False,
}
