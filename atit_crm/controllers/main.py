from odoo import http
from odoo.http import request
import werkzeug, json


REQUIRED_FIELDS = ['name', 'partner', 'mobile', 'email', 'function', 'employees_number']
EMPLOYEES = [('1','1~10'),('2','11~50'),('3','51~100'),('4','101~200'),('5','201~500'),('6','501~1000'),('7','>1000')]


class ATIT_CRM(http.Controller):

    @http.route('/api/lead', type='http', method=['POST'], auth='public', csrf=False)
    def add_lead(self, **post):
        print('//////' , post)
        result = {
            'result': 'sucess',
            'field': '',
            'message': 'Data is correct',
        }
        for field in REQUIRED_FIELDS:
            if not post[field]:
                result['result'] = 'fail'
                result['field'] = field
                result['message'] = 'required field is empty'
                return werkzeug.wrappers.Response(
                    status=401,
                    content_type='application/json; charset=utf-8',
                    response=json.dumps(result),
                )
        vals = {
            'name': str(post.get('partner')),
            'contact_name': post.get('name', ''),
            'partner_name': post.get('partner', ''),
            'mobile': post.get('mobile', ''),
            'email_from': post.get('email', ''),
            'function': post.get('function', ''),
            'employees_number': post.get('employees_number', ''),
            'use_odoo': post.get('use_odoo', False),
            'company_activity': post.get('company_activity', ''),
            'description': post.get('note', ''),
            'type': 'opportunity',
            'odoo_lead': True,
            'module_discuss': post.get('module_discuss', False),
            'module_note': post.get('module_note', False),
            'module_calendar': post.get('module_calendar', False),
            'module_contact': post.get('module_contact', False),
            'module_crm': post.get('module_crm', False),
            'module_sale': post.get('module_sale', False),
            'module_subscription': post.get('module_subscription', False),
            'module_marketing': post.get('module_marketing', False),
            'module_website': post.get('module_website', False),
            'module_document': post.get('module_document', False),
            'module_sms_marketing': post.get('module_sms_marketing', False),
            'module_auto_marketing': post.get('module_auto_marketing', False),
            'module_sign': post.get('module_sign', False),
            'module_elearning': post.get('module_elearning', False),
            'module_rental': post.get('module_rental', False),
            'module_pos': post.get('module_pos', False),
            'module_planning': post.get('module_planning', False),
            'module_helpdesk': post.get('module_helpdesk', False),
            'module_inventory': post.get('module_inventory', False),
            'module_purchase': post.get('module_purchase', False),
            'module_manufacturing': post.get('module_manufacturing', False),
            'module_barcode': post.get('module_barcode', False),
            'module_quality': post.get('module_quality', False),
            'module_prod_life_cycle': post.get('module_prod_life_cycle', False),
            'module_maintenance': post.get('module_maintenance', False),
            'module_repair': post.get('module_repair', False),
            'module_accounting': post.get('module_accounting', False),
            'module_consolidation': post.get('module_consolidation', False),
            'module_project': post.get('module_project', False),
            'module_payroll': post.get('module_payroll', False),
            'module_timesheet': post.get('module_timesheet', False),
            'module_field_service': post.get('module_field_service', False),
            'module_email_marketing': post.get('module_email_marketing', False),
            'module_event': post.get('module_event', False),
            'module_survey': post.get('module_survey', False),
            'module_employee': post.get('module_employee', False),
            'module_attendance': post.get('module_attendance', False),
            'module_recruitement': post.get('module_recruitement', False),
            'module_referral': post.get('module_referral', False),
            'module_time_off': post.get('module_time_off', False),
            'module_approval': post.get('module_approval', False),
            'module_expense': post.get('module_expense', False),
            'module_launch': post.get('module_launch', False),
            'module_fleet': post.get('module_fleet', False),
            'module_live_chat': post.get('module_live_chat', False),
            'module_setting': post.get('module_setting', False),
            'module_dashboard': post.get('module_dashboard', False),
            'module_iot': post.get('module_iot', False),
            'module_voip': post.get('module_voip', False),
            'module_other': post.get('module_other', False),
            'module_ecommerce': post.get('module_ecommerce', False),
        }
        request.env['crm.lead'].sudo().create(vals)
        return werkzeug.wrappers.Response(
            status=200,
            content_type='application/json; charset=utf-8',
            response=json.dumps(result),
        )
