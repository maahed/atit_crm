from odoo import models, fields, api
from ast import literal_eval


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    installation_cost = fields.Float('Installation Cost')
    training_cost = fields.Float('Training Cost/hour')
    implementation_cost = fields.Float('Implementation Cost/hour')
    customization_cost = fields.Float('Customization Cost/hour')
    integration_cost = fields.Float('Integration Cost/hour')
    server_support = fields.Float('Server Support Cost')
    project_management = fields.Float('PM cost/hour')
    requirement = fields.Float('Requirement Analysis cost/hour')

    installation_product = fields.Many2one('product.product', string='Installation Product', config_parameter='default_installation_product_id')
    integration_product = fields.Many2one('product.product', string='Integration Product', config_parameter='default_integration_product_id')
    customization_product = fields.Many2one('product.product', string='Customization Product', config_parameter='default_customization_product_id')
    training_product = fields.Many2one('product.product', string='Training Product', config_parameter='default_training_product_id')
    implementation_product = fields.Many2one('product.product', string='Implementation Product', config_parameter='default_implementation_product_id')
    server_product = fields.Many2one('product.product', string='Server Product', config_parameter='default_server_product_id')
    server_support_product = fields.Many2one('product.product', string='Support Product', config_parameter='default_server_support_product_id')
    project_management_product = fields.Many2one('product.product', string='PM Product', config_parameter='default_project_management_product_id')
    requirement_product = fields.Many2one('product.product', string='Requirement Product', config_parameter='default_requirement_product_id')
    odoo_license_product = fields.Many2one('product.product', string='Odoo License Product', config_parameter='default_odoo_license_product_id')
    magento_product = fields.Many2one('product.product', string='Magento Product', config_parameter='default_magento_product_id')
    extra_charge_product = fields.Many2one('product.product', string='Extra Charge Product', config_parameter='default_extra_charge_product_id')

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        set_param = self.env['ir.config_parameter'].set_param
        set_param('installation_cost', (self.installation_cost or 1000))
        set_param('customization_cost', (self.customization_cost or 100))
        set_param('integration_cost', (self.integration_cost or 100))
        set_param('training_cost', (self.training_cost or 200))
        set_param('implementation_cost', self.implementation_cost)
        set_param('server_support', self.server_support)
        set_param('project_management', self.project_management)
        set_param('requirement', self.requirement)

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        get_param = self.env['ir.config_parameter'].sudo().get_param
        res.update(
            installation_cost=literal_eval(get_param('installation_cost', default=1000)),
            customization_cost=literal_eval(get_param('customization_cost', default=100)),
            integration_cost=literal_eval(get_param('integration_cost', default=100)),
            training_cost=literal_eval(get_param('training_cost', default=200)),
            implementation_cost=literal_eval(get_param('implementation_cost', default=250)),
            server_support=literal_eval(get_param('server_support', default=500)),
            project_management=literal_eval(get_param('project_management')),
            requirement=literal_eval(get_param('requirement')),
        )
        return res
