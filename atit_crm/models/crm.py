from odoo import models, fields, api, _


class CRM(models.Model):
    _inherit = 'crm.lead'

    def new_quote(self):
        get_param = self.env['ir.config_parameter'].sudo().get_param
        form_view = self.env.ref('sale.view_order_form')
        list = [{
                    'product_id': int(get_param('default_installation_product_id')),
                    'name': _('Installation'),
                    'product_uom_qty': 1,
                    'price_unit': get_param('installation_cost'),
                    'product_uom': self.env['product.product'].browse(int(get_param('default_installation_product_id'))).uom_id.id
                },{
                    'product_id': int(get_param('default_server_product_id')),
                    'name': _('Server Cost'),
                    'product_uom_qty': self.support_period,
                    'price_unit': self.server_cost,
                    'product_uom': self.env['product.product'].browse(int(get_param('default_server_product_id'))).uom_id.id
                }]
        if self.hours_implementation:
            list.append({
                    'product_id': int(get_param('default_implementation_product_id')),
                    'name': _('Implementation'),
                    'product_uom_qty': self.hours_implementation,
                    'price_unit': get_param('implementation_cost'),
                    'product_uom': self.env['product.product'].browse(int(get_param('default_implementation_product_id'))).uom_id.id
                })
        if self.support_period:
            list.append({
                    'product_id': int(get_param('default_server_support_product_id')),
                    'name': _('Server Support'),
                    'product_uom_qty': self.support_period,
                    'price_unit': get_param('server_support'),
                    'product_uom': self.env['product.product'].browse(int(get_param('default_server_support_product_id'))).uom_id.id
                })
        if self.hours_number:
            list.append({
                    'product_id': int(get_param('default_training_product_id')),
                    'name': _('Training'),
                    'product_uom_qty': self.hours_number,
                    'price_unit': get_param('training_cost'),
                    'product_uom': self.env['product.product'].browse(int(get_param('default_training_product_id'))).uom_id.id
                })
        if self.hours_customization:
            list.append({
                'product_id': int(get_param('default_customization_product_id')),
                'name': _('Customization'),
                'product_uom_qty': self.hours_customization,
                'price_unit': get_param('customization_cost'),
                'product_uom': self.env['product.product'].browse(int(get_param('default_customization_product_id'))).uom_id.id
            })
        if self.hours_integration:
            list.append({
                'product_id': int(get_param('default_integration_product_id')),
                'name': _('Integration'),
                'product_uom_qty': self.hours_integration,
                'price_unit': get_param('integration_cost'),
                'product_uom': self.env['product.product'].browse(int(get_param('default_integration_product_id'))).uom_id.id
            })
        if self.project_management:
            list.append({
                'product_id': int(get_param('default_project_management_product_id')),
                'name': _('Project Management'),
                'product_uom_qty': self.project_management,
                'price_unit': get_param('project_management'),
                'product_uom': self.env['product.product'].browse(int(get_param('default_project_management_product_id'))).uom_id.id
            })
        if self.requirement:
            list.append({
                'product_id': int(get_param('default_requirement_product_id')),
                'name': _('Requirement Analysis'),
                'product_uom_qty': self.requirement,
                'price_unit': get_param('requirement'),
                'product_uom': self.env['product.product'].browse(int(get_param('default_requirement_product_id'))).uom_id.id
            })
        if self.odoo_license:
            list.append({
                'product_id': int(get_param('default_odoo_license_product_id')),
                'name': _('Odoo License'),
                'product_uom_qty': 1,
                'price_unit': self.odoo_license,
                'product_uom': self.env['product.product'].browse(int(get_param('default_odoo_license_product_id'))).uom_id.id
            })
        if self.magento:
            list.append({
                'product_id': int(get_param('default_magento_product_id')),
                'name': _('Magento'),
                'product_uom_qty': 1,
                'price_unit': self.magento,
                'product_uom': self.env['product.product'].browse(int(get_param('default_magento_product_id'))).uom_id.id
            })
        if self.extra_charge:
            list.append({
                'product_id': int(get_param('default_extra_charge_product_id')),
                'name': _('Extra Charges'),
                'product_uom_qty': self.extra_charge,
                'price_unit': 1,
                'product_uom': self.env['product.product'].browse(int(get_param('default_extra_charge_product_id'))).uom_id.id
            })
        print('return : ', list)
        return {
            'name': _('New Quotation'),
            'res_model': 'sale.order',
            'view_mode': 'form',
            'view_type': 'form',
            'views': [(form_view.id, 'form')],
            'view_id': form_view.id,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context':  {
                'default_partner_id': self.partner_id.id,
                'default_team_id': self.team_id.id,
                'default_origin': self.name,
                'default_order_line': [(0, 0, x) for x in list]
            }
        }

    @api.multi
    def compute_server_cost(self):
        if self.server_type == 'none':
            self.server_cost = 0
            return
        if self.users_number in range(1, 15):
            if self.server_type == 'digital':
                self.server_cost = 300
            if self.server_type == 'google':
                if self.module_accounting and self.module_pos and self.module_employee:
                    self.server_cost = 695
                if self.module_accounting and (self.module_pos or self.module_employee):
                    self.server_cost = 655
                else:
                    self.server_cost = 300

        if self.users_number in range(16, 30):
            if self.server_type == 'digital':
                if not self.module_accounting and (self.module_pos or self.module_employee):
                    self.server_cost = 600
                else:
                    self.server_cost = 300
            if self.server_type == 'google':
                if self.module_accounting and self.module_pos and self.module_employee:
                    self.server_cost = 1305
                if self.module_accounting and self.module_pos and not self.module_employee:
                    self.server_cost = 1280
                if self.module_accounting and self.module_employee and not self.module_pos:
                    self.server_cost = 695
                else:
                    self.server_cost = 655

        if self.users_number in range(31, 100):
            if self.server_type == 'digital':
                self.server_cost = 1200
            if self.server_type == 'google':
                if self.module_accounting and self.module_pos and self.module_employee:
                    self.server_cost = 2685
                if self.module_accounting and self.module_pos and not self.module_employee:
                    self.server_cost = 2605
                if self.module_accounting and self.module_employee and not self.module_pos:
                    self.server_cost = 2605
                else:
                    self.server_cost = 1390

        if self.users_number in range(101, 200):
            if self.server_type == 'digital':
                if self.module_accounting and self.module_pos and self.module_employee:
                    self.server_cost = 3750
                if self.module_accounting and self.module_pos:
                    self.server_cost = 2400
                else:
                    self.server_cost = 1200
            if self.server_type == 'google':
                if self.module_accounting and self.module_pos and self.module_employee:
                    self.server_cost = 4185
                if self.module_accounting and self.module_pos and not self.module_employee:
                    self.server_cost = 4000
                if self.module_accounting and self.module_employee and not self.module_pos:
                    self.server_cost = 2765
                else:
                    self.server_cost = 2685

        if self.users_number in range(201, 300):
            if self.server_type == 'digital':
                if self.module_accounting and self.module_pos and self.module_employee:
                    self.server_cost = 7800
                if self.module_accounting and self.module_pos and not self.module_employee:
                    self.server_cost = 4800
                if self.module_accounting and self.module_employee and not self.module_pos:
                    self.server_cost = 2700
                else:
                    self.server_cost = 2400
            if self.server_type == 'google':
                if self.module_accounting and self.module_pos and self.module_employee:
                    self.server_cost = 9595
                if self.module_accounting and self.module_pos and not self.module_employee:
                    self.server_cost = 8000
                else:
                    self.server_cost = 4840
        if self.users_number > 300:
            self.server_cost = 0



    employees_number = fields.Selection([('1', '1~10'),('2', '11~50'),('3', '51~100'),('4', '101~200'),('5', '201~500'),('6', '501~1000'),('7', '>1000')],
                                        string='Employees Number', default='1')
    use_odoo = fields.Boolean('Are you using Odoo ?')
    company_activity = fields.Char('Company activity')

    users_number = fields.Integer('Users Number', default=5)
    hours_number = fields.Integer('Training Hours', default=20)
    hours_implementation = fields.Integer('Implementation Hours', default=20)
    hours_integration = fields.Integer('Integration Hours', default=20)
    hours_customization = fields.Integer('Customization Hours', default=20)
    support_period = fields.Integer('Support Period/month', default=12)
    server_type = fields.Selection([('none', 'None'),('digital', 'Digital Ocean'),('google', 'Google Cloud')], string='Server', default='google', required=True)
    project_management = fields.Integer('Project Management', default=20)
    requirement = fields.Integer('Requirement Analysis', default=20)

    odoo_license = fields.Float('Odoo License Cost')
    magento = fields.Float('Magento Cost')
    risk = fields.Float('Risk(%)')
    extra_charge = fields.Float('Extra Charge')
    server_cost = fields.Float('Server Cost', compute=lambda s: s.compute_server_cost())

    odoo_lead = fields.Boolean('Odoo Lead')

    module_discuss = fields.Boolean('')
    module_note = fields.Boolean('')
    module_calendar = fields.Boolean('')
    module_contact = fields.Boolean('')
    module_crm = fields.Boolean('')
    module_sale = fields.Boolean('')
    module_subscription = fields.Boolean('')
    module_marketing = fields.Boolean('')
    module_website = fields.Boolean('')
    module_document = fields.Boolean('')
    module_sms_marketing = fields.Boolean('')
    module_auto_marketing = fields.Boolean('')
    module_sign = fields.Boolean('')
    module_elearning = fields.Boolean('')
    module_rental = fields.Boolean('')
    module_pos = fields.Boolean('')
    module_planning = fields.Boolean('')
    module_helpdesk = fields.Boolean('')
    module_inventory = fields.Boolean('')
    module_purchase = fields.Boolean('')
    module_manufacturing = fields.Boolean('')
    module_barcode = fields.Boolean('')
    module_quality = fields.Boolean('')
    module_prod_life_cycle = fields.Boolean('')
    module_maintenance = fields.Boolean('')
    module_repair = fields.Boolean('')
    module_accounting = fields.Boolean('')
    module_consolidation = fields.Boolean('')
    module_project = fields.Boolean('')
    module_payroll = fields.Boolean('')
    module_timesheet = fields.Boolean('')
    module_field_service = fields.Boolean('')
    module_email_marketing = fields.Boolean('')
    module_event = fields.Boolean('')
    module_survey = fields.Boolean('')
    module_employee = fields.Boolean('')
    module_attendance = fields.Boolean('')
    module_recruitement = fields.Boolean('')
    module_referral = fields.Boolean('')
    module_time_off = fields.Boolean('')
    module_approval = fields.Boolean('')
    module_expense = fields.Boolean('')
    module_launch = fields.Boolean('')
    module_fleet = fields.Boolean('')
    module_live_chat = fields.Boolean('')
    module_setting = fields.Boolean('')
    module_dashboard = fields.Boolean('')
    module_iot = fields.Boolean('')
    module_voip = fields.Boolean('')
    module_other = fields.Boolean('')
    module_ecommerce = fields.Boolean('')

